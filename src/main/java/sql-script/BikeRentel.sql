/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  student
 * Created: 17-Jun-2022
 */

CREATE DATABASE BIKERENTEL;
USE BIKERENTEL;
CREATE TABLE BIKE(ID INT PRIMARY KEY AUTO_INCREMENT,BIKE_REG INT NOT NULL,MODEL VARCHAR (15) NOT NULL,PRICE DECIMAL (10,2) NOT NULL);
DESC BIKE;

CREATE TABLE CUSTOMER(ID INT PRIMARY KEY AUTO_INCREMENT,CUST_NAME VARCHAR(15) NOT NULL,LICENCE VARCHAR(10) NOT NULL,CUST_MOB VARCHAR (10) NOT NULL);
DESC CUSTOMER;

CREATE TABLE RRR(ID INT PRIMARY KEY AUTO_INCREMENT,RENT_DATE DATE NOT NULL,RETURN_DATE DATE NOT NULL,FEES DECIMAL (10,2) NOT NULL,BIKE_ID INT,FOREIGN KEY(BIKE_ID) REFERENCES BIKE(ID),CUST_ID INT,FOREIGN KEY(CUST_ID) REFERENCES CUSTOMER(ID));
DESC RRR;

