/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bikerentel.ui.data;

/**
 *
 * @author student
 */
public class CustomerDetails {

    private Integer id;
    private String CustName;
    private String Licence;
    private Integer CustMob;

    public CustomerDetails (Integer id, String CustName, String Licence, Integer CustMob) {
        this.id = id;
        this.CustName = CustName;
        this.Licence = Licence;
        this.CustMob = CustMob;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustName() {
        return CustName;
    }

    public void setCustName(String CustName) {
        this.CustName = CustName;
    }

    public String getLicence() {
        return Licence;
    }

    public void setLicence(String Licence) {
        this.Licence = Licence;
    }

    public Integer getCustMob() {
        return CustMob;
    }

    public void setCustMob(Integer CustMob) {
        this.CustMob = CustMob;
    }
}
