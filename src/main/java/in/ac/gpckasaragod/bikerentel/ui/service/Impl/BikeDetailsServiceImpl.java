/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bikerentel.ui.service.Impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import in.ac.gpckasaragod.bikerentel.service.BikeDetailsService;
import in.ac.gpckasaragod.bikerentel.ui.data.BikeDetails;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author student
 */
public class BikeDetailsServiceImpl extends ConnectionServiceImpl implements BikeDetailsService {

    @Override
    public String saveBike(String BikeReg, String Model, Double Price) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO BIKE(BIKE_REG,MODEL,PRICE) VALUES " + "('" + BikeReg + "','" + Model + "','" + Price + "')";
            System.out.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Saved Failed";
            } else {
                return "Saved Successfully";
            }

        } catch (SQLException ex) {
            Logger.getLogger(BikeDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Saved Failed";
        }
    }
    @Override
    public BikeDetails readBikeDetails(Integer Id) {
       BikeDetailsService bike =null;
        try{
            Connection connection =getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM  BIKE WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                Integer BikeReg = resultSet.getInt("BIKE_REG");
                String Model = resultSet.getString("MODEL");
                Double Price = resultSet.getDouble("PRICE");
                bike = (BikeDetailsService) new BikeDetails(id,BikeReg,Price,Model);
               
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(BikeDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            
        }
        return (BikeDetails) bike;    
        
    }

    @Override
    public List<BikeDetailsService> getAllBikeDetails() {
          List<BikeDetailsService> bikes = new ArrayList<>();
    try{
        Connection connection =getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM BIKES";
    ResultSet resultSet = statement.executeQuery(query);
     while(resultSet.next()){
        int id = resultSet.getInt("ID");
              
                Integer BikeReg = resultSet.getInt("BIKE_REG");
                String Model = resultSet.getString("MODEL");
                Double Price= resultSet.getDouble("PRICE");
                BikeDetails bike = new BikeDetails (id,BikeReg,Model,Price);
               bikes.add((BikeDetailsService) bike);
     }
        
    }   catch (SQLException ex) {
            Logger.getLogger(BikeDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bikes; 
    }

    @Override
    public String updateBikeDetails(Integer id, String BikeReg, String Model, Double Price) {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try{
            Connection connection =getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE BIKE SET BIKE_REG ='"+BikeReg+"',MODEL ='"+Model+"',PRICE = '"+Price+"' WHERE ID="+id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
            return "Update Failed";
            else
                return "Updated successfully";
            
    }   catch (SQLException ex) {
        return "Updated Failed";
            
        }
    }

    @Override
    public String deleteBikeDetails(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try{
            Connection connection =getConnection();
            String query = "DELETE FROM BIKE WHERE ID=?";
            PreparedStatement statement = connection.prepareCall(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if (delete !=1)
                return "Delete Failed";
            else
                return "Deleted Successfully";
            
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return "Deleted Failed";
    }
  
    }

    @Override
    public String saveBikeDetail(String BikeReg, String Model, String Price) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO BIKE(BIKE_REG,MODEL,PRICE) VALUES " + "('" + BikeReg + "','" + Model + "','" + Price + "')";
            System.out.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Saved Failed";
            } else {
                return "Saved Successfully";
            }

        } catch (SQLException ex) {
            Logger.getLogger(BikeDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Saved Failed";
        }
    }

    @Override
    public String updateBikeDetaill(Integer selectedId, String BikeReg, String Model, String Price) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String deleteBike(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    
}
