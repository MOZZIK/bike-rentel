/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bikerentel.ui.data;

import java.sql.Date;

/**
 *
 * @author student
 */
public class RrDetails {

    private Integer id;
    private Integer BikeId;
    private Integer CustId;
    private Date RentDate;
    private Date ReturnDate;
    private Integer Fees;

    public RrDetails (Integer id, Integer BikeId, Integer CustId, Date RentDate,Date ReturnDate,Integer Fees) {
        this.id = id;
        this.BikeId = BikeId;
        this.CustId = CustId;
        this.RentDate = RentDate;
        this.ReturnDate = ReturnDate;
        this.Fees = Fees;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBikeId() {
        return BikeId;
    }

    public void setBikeId(Integer BikeId) {
        this.BikeId = BikeId;
    }

    public Integer getCustId() {
        return CustId;
    }

    public void setCustId(Integer CustId) {
        this.CustId = CustId;
    }

    public Date getRentDate() {
        return RentDate;
    }

    public void setRentDate(Date RentDate) {
        this.RentDate = RentDate;
    }

    public Date getReturnDate() {
        return ReturnDate;
    }

    public void setReturnDate(Date ReturnDate) {
        this.ReturnDate = ReturnDate;
    }

    public Integer getFees() {
        return Fees;
    }

    public void setFees(Integer Fees) {
        this.Fees = Fees;
    }
}
