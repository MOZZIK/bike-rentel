/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bikerentel.ui.data;

/**
 *
 * @author student
 */
public class BikeDetails {
    private Integer id;
    private String BikeReg;
    private String Model;
    private Double Price;

    public BikeDetails(Integer id, String BikeReg, String Model, Double Price) {
        this.id = id;
        this.BikeReg = BikeReg;
        this.Model = Model;
        this.Price = Price;
    }

    public BikeDetails(Integer id, Integer BikeReg, Double Price, String Model) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public BikeDetails(int id, Integer BikeReg, String Model, Double Price) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBikeReg() {
        return BikeReg;
    }

    public void setBikeReg(String BikeReg) {
        this.BikeReg = BikeReg;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double Price) {
        this.Price = Price;
    }
}
