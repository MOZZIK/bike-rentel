/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.bikerentel.service;

import java.util.List;

/**
 *
 * @author student
 */
public interface CustomerDetailsService {
    public String CustomerDetails(String saloonRegno,String saloonName,String barberName);
    public CustomerDetailsService readCustomer(Integer Id);
    public List<CustomerDetailsService>getAllCustomer();
    public String updateBike(Integer id, String BikeReg, String Model, Double Price);
    public String deleteBIKE(Integer id) ;
}
   
