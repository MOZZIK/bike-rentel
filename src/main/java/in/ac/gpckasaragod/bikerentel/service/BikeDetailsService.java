/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.bikerentel.service;

import in.ac.gpckasaragod.bikerentel.ui.data.BikeDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface BikeDetailsService {
    public String saveBike(String BikeReg,String Model,Double Price);
    public BikeDetails readBikeDetails(Integer Id);
    public List<BikeDetailsService>getAllBikeDetails();
    public String updateBikeDetails(Integer id, String BikeReg, String Model, Double Price);
    public String deleteBikeDetails(Integer id) ;

    public String saveBikeDetail(String BikeReg, String Model, String Price);

    public String updateBikeDetaill(Integer selectedId, String BikeReg, String Model, String Price);

    public String deleteBike(Integer id);
}

